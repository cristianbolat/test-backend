class CustomersController < ApplicationController
  before_action :set_customer, only: [:show]

  # GET /customers/
  def index
  	@customers = Customer.all
    json_response(@customers)
  end
end
