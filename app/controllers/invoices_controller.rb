class InvoicesController < ApplicationController
  before_action :set_customer
  before_action :set_customer_invoice, only: [:show, :update]

  # GET /customers/:customer_id/invoices
  def index
    json_response(@customer.invoices)
  end

  # POST /customers/:customer_id/invoices
  def create
    @customer.invoices.create!(invoice_params)
    json_response(@customer, :created)
  end

  # PUT /customers/:customer_id/invoices/:id
  def update
    @invoice.update(invoice_params)
    head :no_content
  end

  private

  def invoice_params
    params.permit(:invoice_number, :date, :due_date, :amount)
  end

  def set_customer
    @customer = Customer.find(params[:customer_id])
  end

  def set_customer_invoice
    @invoice= @customer.invoices.find_by!(id: params[:id]) if @customer
  end
end
