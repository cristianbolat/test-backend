class Customer < ApplicationRecord
  # model association
  has_many :invoices, dependent: :destroy

  # validations
  validates_presence_of :company_name, :email_address
end
