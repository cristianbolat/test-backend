class Invoice < ApplicationRecord
  # model association
  belongs_to :customer

  # validation
  validates_presence_of :invoice_number, :date, :due_date, :amount
end
