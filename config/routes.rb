Rails.application.routes.draw do
  get '/customers', to: 'customers#index'
  get '/customers/:customer_id/invoices', to: 'invoices#index'
  post '/customers/:customer_id/invoices', to: 'invoices#create'
  put '/customers/:customer_id/invoices/:id', to: 'invoices#update'

  post 'auth/login', to: 'authentication#authenticate'
  post 'signup', to: 'users#create'
end
