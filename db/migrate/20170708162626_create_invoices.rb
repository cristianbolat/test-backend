class CreateInvoices < ActiveRecord::Migration[5.1]
  def change
    create_table :invoices do |t|
      t.numeric :invoice_number
      t.references :customer, foreign_key: true
      t.date :date
      t.date :due_date
      t.numeric :amount

      t.timestamps
    end
  end
end
