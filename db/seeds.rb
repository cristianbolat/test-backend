# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

# ruby encoding: utf-8

Customer.create(company_name: 'Oracle', email_address: 'exampe@exampe.com')
Customer.create(company_name: 'BA', email_address: 'exampe@exampe.com')
Customer.create(company_name: 'ZIT', email_address: 'exampe@exampe.com')
Customer.create(company_name: 'BASF', email_address: 'exampe@exampe.com')
