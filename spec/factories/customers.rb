FactoryGirl.define do
  factory :customer do
    company_name { Faker::Lorem.word }
    email_address { Faker::Internet.email }
  end
end