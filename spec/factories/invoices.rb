FactoryGirl.define do
  factory :invoice do
    invoice_number { Faker::Number.number(10) }
    date { Faker::Time.forward(2, :morning) }
    due_date { Faker::Time.forward(3, :morning) }
    amount { Faker::Number.number(10) }
  end
end