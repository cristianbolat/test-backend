require 'rails_helper'

RSpec.describe Customer, type: :model do
  # Association test
  # ensure Customer model has a 1:m relationship with the Invoice model
  it { should have_many(:invoices).dependent(:destroy) }
  # Validation tests
  # ensure columns company_name and email_address are present before saving
  it { should validate_presence_of(:company_name) }
  it { should validate_presence_of(:email_address) }
end
