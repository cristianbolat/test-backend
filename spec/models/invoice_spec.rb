require 'rails_helper'

RSpec.describe Invoice, type: :model do
  # Association test
  # ensure an invoice record belongs to a single customer record
  it { should belong_to(:customer) }
  # Validation test
  # ensure columns invoice_number date, due_date and amount are present before saving
  it { should validate_presence_of(:invoice_number) }
  it { should validate_presence_of(:date) }
  it { should validate_presence_of(:due_date) }
  it { should validate_presence_of(:amount) }
end
