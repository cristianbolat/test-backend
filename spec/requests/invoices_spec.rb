require 'rails_helper'

RSpec.describe 'Invoices API', type: :request do
  # initialize test data 
  let(:user) { create(:user) }
  let!(:customer) { create(:customer) }
  let!(:invoices) { create_list(:invoice, 20, customer_id: customer.id ) }
  let(:customer_id) {customer.id }
  let(:id) { invoices.first.id }

  # authorize request
  let(:headers) { valid_headers }

  # Test suite for GET /customers/:customer_id/invoices
  describe 'GET /customers/:customer_id/invoices' do
    before { get "/customers/#{customer_id}/invoices", params: {}, headers: headers }

    context 'when customer exists' do
      it 'returns status code 200' do
        expect(response).to have_http_status(200)
      end

      it 'returns all customer invoices' do
        expect(json.size).to eq(20)
      end
    end

    context 'when customer does not exist' do
      let(:customer_id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Customer/)
      end
    end
  end

  # Test suite for POST /customers/:customer_id/invoices
  describe 'POST /customers/:customer_id/invoices' do
    # valid payload
    let(:valid_attributes) { { invoice_number: '10', date: Date.today, due_date: Date.today, amount: '1000' }.to_json }

    context 'when the request is valid' do
      before do
        post "/customers/#{customer_id}/invoices", params: valid_attributes, headers: headers
      end

      it 'returns status code 201' do
        expect(response).to have_http_status(201)
      end
    end

    context 'when the request is invalid' do
      before { post "/customers/#{customer_id}/invoices", params: {}, headers: headers }

      it 'returns status code 422' do
        expect(response).to have_http_status(422)
      end

      it 'returns a validation failure message' do
        expect(response.body)
          .to match(/Validation failed: Invoice number can't be blank, Date can't be blank, Due date can't be blank, Amount can't be blank/)
      end
    end
  end

  # Test suite for PUT /customers/:customer_id/invoices/:id
  describe 'PUT /customers/:customer_id/invoices/:id' do
    let(:valid_attributes) { { amount: '2000' }.to_json }

    before do
      put "/customers/#{customer_id}/invoices/#{id}", params: valid_attributes, headers: headers
    end

    context 'when invoice exists' do
      it 'returns status code 204' do
        expect(response).to have_http_status(204)
      end

      it 'updates the invoice' do
        updated_invoice = Invoice.find(id)
        expect(updated_invoice.amount).to match(2000)
      end
    end

    context 'when the invoice does not exist' do
      let(:id) { 0 }

      it 'returns status code 404' do
        expect(response).to have_http_status(404)
      end

      it 'returns a not found message' do
        expect(response.body).to match(/Couldn't find Invoice/)
      end
    end
  end
end